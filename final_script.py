# import utilities
import pandas as pd
import numpy as np
import scipy as sp
from copy import deepcopy

from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.model_selection import train_test_split, GridSearchCV, StratifiedKFold, ParameterGrid
from sklearn.metrics import confusion_matrix, f1_score, accuracy_score, matthews_corrcoef, make_scorer


# leaning Algorithms
from sklearn.svm import SVC
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
import xgboost as xgb
from xgboost.sklearn import XGBClassifier
from imblearn.over_sampling import SMOTE
import warnings
warnings.filterwarnings("ignore")
def report_gird_cv_statistics(model, name, X_train, y_train):
    '''
    A simple function that returns a string containing some statistics of the Grid Search results
    '''
    """
    A function used to report the result of GridSearchCV
    """
    report = f'{name} with best parameters:\n {model.best_params_} \n resulted in the following:\n \
    ----------\n \
    best MCC validation score (mean) on validation fold: {model.best_score_:.4f} \
    with std: {model.cv_results_["std_test_MCC"][model.best_index_]:.4f}\n \
    with corresponding MCC score on taining folds: {model.cv_results_["mean_train_MCC"][model.best_index_]:.4f}\n \
    ----------\n \
    corresponding validation F_score (mean): {model.cv_results_["mean_test_f_score"][model.best_index_]:.4f}\n \
    with corresponding training f_score : {model.cv_results_["mean_train_f_score"][model.best_index_]:.4f}\n \
    confusion matrix on training date:\n {confusion_matrix(y_true=y_train, y_pred=model.predict(X_train))}'
    return report


if __name__ == '__main__':

    # Define the dimensions
    num_visualization_dimention = 2
    num_dimention = 50
    num_dimention_extended = 2000
    print('Reading the data')
    # load the data
    train = pd.read_csv('./data/train_data.csv', header=None)
    train_lables = pd.read_csv('./data/train_labels.csv', header=None, names=['lable'])
    test = pd.read_csv('./data/test_data.csv', header=None)

    print('performing PCA')
    pca = PCA()
    pca_result = pca.fit_transform(train.values)
    pca_test_result = pca.transform(test)  # perform PCA using the same pca transform


    print(f'Explained variance by the first {num_visualization_dimention} PCA components {np.sum(pca.explained_variance_ratio_[0:num_visualization_dimention])}\n\
    Explained variance by the first 3 PCA components {np.sum(pca.explained_variance_ratio_[0:3])}\n\
    Explained variance by the first {num_dimention} PCA components {np.sum(pca.explained_variance_ratio_[0:num_dimention])} \n\
    Explained variance by the first {num_dimention_extended} {np.sum(pca.explained_variance_ratio_[0:num_dimention_extended])}')

    # perform train test split to be used by different algorithms
    X_train, X_held_out, y_train, y_held_out = train_test_split(pca_result[:, 0:num_dimention_extended],
                                                                train_lables['lable'].values,
                                                                test_size=0.2,
                                                                random_state=42,
                                                                )

    print(f'minority class ratio in the train dataset:{np.unique(y_held_out, return_counts=True)[1].min()/float(np.unique(y_held_out, return_counts=True)[1].sum()):.4f}')
    print(f'minority class ratio in the held_out dataset:{np.unique(y_train, return_counts=True)[1].min()/float(np.unique(y_train, return_counts=True)[1].sum()):.4f}')

    # These scorers will be used for CV (if needed)
    MCC_scorer = make_scorer(matthews_corrcoef)
    f_scorer = make_scorer(f1_score)
    score_dict = {'MCC': MCC_scorer, 'f_score': f_scorer}

    print('Cross validation SVM for taining set')
    SVM_params_1 = {'class_weight': ['balanced', None],  # check both optiones for being balanced and unbalanced
                    'kernel': ['rbf'],
                    'gamma': np.logspace(-13, -10, 10),
                    'C': np.logspace(2, 5, 10)}

    print('Tuning hyper parameters')
    svm_rbf_50_1 = GridSearchCV(SVC(),
                                SVM_params_1,
                                return_train_score=True,
                                cv=5,
                                scoring=score_dict, refit='MCC',
                                n_jobs=6)  # Grid Search creates stratified folds
    svm_rbf_50_1.fit(X_train[:, 0:num_dimention], y_train)

    print(report_gird_cv_statistics(model=svm_rbf_50_1, name='SVM(rbf) with 50 dimentions Attempt 1',
                                    X_train=X_train[:, 0:num_dimention],
                                    y_train=y_train))

    print(f'svm_rbf_50_1 MCC score on held out dataset{matthews_corrcoef(y_true=y_held_out, y_pred=svm_rbf_50_1.predict(X_held_out[:, 0:num_dimention]))}')
    print(f'svm F1 score score on held out dtaset{f1_score(y_true=y_held_out, y_pred=svm_rbf_50_1.predict(X_held_out[:, 0:num_dimention]))}')
    print(confusion_matrix(y_true=y_held_out, y_pred=svm_rbf_50_1.predict(X_held_out[:, 0:num_dimention])))

    print('Done with cross validation and training the final model')
    svm_rbf_50_final = SVC(**svm_rbf_50_1.best_estimator_.get_params())
    # train on the whole training set
    svm_rbf_50_final.fit(pca_result[:, 0:num_dimention], train_lables['lable'].values)
    test_prediction = svm_rbf_50_final.predict(pca_test_result[:, 0:num_dimention])
    value_counts = np.unique(test_prediction, return_counts=True)
    print(f'minority class prediction ration in the test set {value_counts[1][0]/float(np.sum(value_counts[1]))}')
    np.savetxt('./test_labels.csv', test_prediction, fmt="%d")


